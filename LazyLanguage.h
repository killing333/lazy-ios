//
//  LazyLanguage.h
//  MGIA
//
//  Created by Ho Lun Wan on 18/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

/// Short hand for NSLocalizedString
#define locStr(v)                                  ([[LazyLanguage sharedInstance] stringForKey:v default:v])

/**
 *	Notifications
 */
extern NSString *LazyLanguageNotificationDidChangeLanguage;


@interface LazyLanguage : NSObject

+ (instancetype)sharedInstance;

/**
 *	Return localized string for key in a specific language bundle
 */
+ (NSString *)stringForKey:(NSString *)key language:(NSString *)language default:(NSString *)defaultValue;

/**
 *	Return string for key in the localized strings file
 */
- (NSString *)stringForKey:(NSString *)key default:(NSString *)defaultValue;


@property (nonatomic, readonly) NSBundle *selectedBundle;
@property (nonatomic) NSString *language;					///< Current language
@end





@interface UIView (LazyLanguage)

/**
 *	Reload text for this object
 */
- (void)reloadText;

@end




@interface UIViewController (LazyLanguage)

/**
 *	Reload text for this object
 */
- (void)reloadText;

@end