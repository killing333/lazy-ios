//
//  UIScrollView+Lazy.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 12/9/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Lazy)

/**
 *	Scroll to particular page
 *
 *	@param		page			Page index
 *	@param		animated		Whether or not the scrolling is animated
 */
- (void)scrollToPage:(NSInteger)page animated:(BOOL)animated;


/**
 *	Current page if paging is enabled.
 *	If paging is disabled, -1 is returned
 */
@property (nonatomic, readonly) NSInteger page;
@end
