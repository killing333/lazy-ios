//
//  UINavigationBar+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 16/11/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "UINavigationBar+Lazy.h"

@implementation UINavigationBar (Lazy)

- (void)setShowBottomBorder:(BOOL)showBottomBorder {
	
	if ( showBottomBorder ) {
		[self setBackgroundImage:nil
				  forBarPosition:UIBarPositionAny
					  barMetrics:UIBarMetricsDefault];
		
		[self setShadowImage:nil];
	} else {
		[self setBackgroundImage:[UIImage new]
				  forBarPosition:UIBarPositionAny
					  barMetrics:UIBarMetricsDefault];
		
		[self setShadowImage:[UIImage new]];
	}
}

@end
