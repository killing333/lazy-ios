//
//  LazyProtocolInterceptor.h
//  MGIA
//
//  Created by Ho Lun Wan on 29/9/15.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *	forward all delegate messages to the regular delegate object, 
 *	except for the ones that you override in your custom subclass.
 *
 *	@ref		http://stackoverflow.com/questions/3498158/intercept-objective-c-delegate-messages-within-a-subclass
 */
@interface LazyProtocolInterceptor : NSObject

/**
 *	Init with a list of protocols to be intercepted
 *
 *	@param		interceptedProtocols			List of (Protocol *)
 */
- (instancetype)initWithInterceptedProtocols:(NSArray *)interceptedProtocols;

@property (nonatomic, readonly, copy) NSArray * interceptedProtocols;
@property (nonatomic, weak) id receiver;
@property (nonatomic, weak) id middleMan;
@end
