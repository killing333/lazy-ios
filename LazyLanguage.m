//
//  LazyLanguage.m
//  MGIA
//
//  Created by Ho Lun Wan on 18/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyLanguage.h"


NSString *LazyLanguageNotificationDidChangeLanguage				= @"LazyLanguageNotificationDidChangeLanguage";

@implementation LazyLanguage

- (instancetype)init {
	self = [super init];
	if ( self ) {
		_selectedBundle = nil;
	}
	return self;
}

+ (instancetype)sharedInstance {
	static LazyLanguage *sharedInstance = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		sharedInstance = [[LazyLanguage alloc] init];
	});
	
	return sharedInstance;
}




#pragma mark Getter

+ (NSBundle *)bundleForLanguage:(NSString *)language {
	NSString *path = [[ NSBundle mainBundle] pathForResource:language ofType:@"lproj" ];
	NSBundle *bundle = nil;
	
	if ( path ) {
		bundle = [NSBundle bundleWithPath:path];
	}
	
	return bundle;
}

+ (NSString *)stringForKey:(NSString *)key language:(NSString *)language default:(NSString *)defaultValue {
	NSString *localizedString = nil;
	NSBundle *bundle = [self bundleForLanguage:language];
	if ( bundle ) {
		localizedString = [bundle localizedStringForKey:key value:defaultValue table:nil];
	} else {
		localizedString = defaultValue;
	}
	return localizedString;
}

- (NSString *)stringForKey:(NSString *)key default:(NSString *)defaultValue {
	NSString *localizedString = nil;
	if ( _selectedBundle ) {
		localizedString = [_selectedBundle localizedStringForKey:key value:defaultValue table:nil];
	} else {
		localizedString = [[NSBundle mainBundle] localizedStringForKey:key value:defaultValue table:nil];
	}
	return localizedString;
}





#pragma mark Setter

- (void)setLanguage:(NSString *)language {
	NSBundle *bundle = [[self class] bundleForLanguage:language];
	
	_selectedBundle = bundle;
	if ( _selectedBundle ) {
		_language = language;
		DLog(@"Language set to %@", language);
		
		[[NSNotificationCenter defaultCenter] postNotificationName:LazyLanguageNotificationDidChangeLanguage object:nil];
	} else {
		DLog(@"Language %@ not found", language);
	}
}




@end





@implementation UIView (LazyLanguage)

- (void)reloadText {
//	[self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//		[obj reloadText];
//	}];
}

@end




@implementation UIViewController (LazyLanguage)

- (void)reloadText {
//	[self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//		[obj reloadText];
//	}];
}

@end
