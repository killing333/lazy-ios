//
//  LazyScrollableToolbar.m
//  WardrobeApp
//
//  Created by Wan Ho Lun on 23/4/15.
//  Copyright (c) 2015 Gary Lee. All rights reserved.
//

#import "LazyScrollableToolbar.h"

#import "UIScrollView+Lazy.h"

@interface LazyScrollableToolbarItem : UIButton

@end

@implementation LazyScrollableToolbarItem

@end




#pragma mark -

const CGFloat LazyScrollableToolbarItemFlexibleLength			= 0;

@interface LazyScrollableToolbar () <UIScrollViewDelegate> {
	UIScrollView *_internalScrollView;
	UIView *_contentView;
	MASConstraint *_contentViewWidthConstraint;
	MASConstraint *_contentViewHeightConstraint;
	
	UIView *_lastItem;
	
	CGFloat _itemsTotalLength;				///< Total width of all the items
	CGFloat _itemsTotalInterval;			///< Total interval between all the items
	UIView *_edgeBorderView;
	
	NSInteger _previousSelectedIndex;
}

- (void)updateScrollIndicator;
- (void)updateScrollViewFrame;
- (void)updateLayout;

@property (nonatomic, readwrite) UIEdgeInsets padding;								///< Padding of this toolbar. Only left and right is useful
@property (nonatomic, readwrite) NSUInteger borderWidth;							///< Top border width. Default 0
@property (strong, nonatomic) UIColor *borderColor;									///< Top border color. Default transparent
@end

@implementation LazyScrollableToolbar

- (void)commonInit {
	_previousSelectedIndex = 0;
	_items = [NSMutableArray new];
	
	// Internal scroll view
	_internalScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
	_internalScrollView.scrollsToTop = NO;
	_internalScrollView.clipsToBounds = NO;
	_internalScrollView.delegate = self;
	[self addSubview:_internalScrollView];
	[_internalScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.edges.equalTo(_internalScrollView.superview);
	}];	
	
	// Content view
	_contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	[_internalScrollView addSubview:_contentView];
	[_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.edges.equalTo(_contentView.superview);
	}];
	
	self.clipsToBounds = YES;
	self.padding = UIEdgeInsetsMake(10, 10, 10, 10);
	self.itemLength = LazyScrollableToolbarItemFlexibleLength;
	self.enablePaging = NO;
	self.itemInterval = 10;
	self.borderWidth = 0.0f;
	self.borderColor = [UIColor clearColor];
	self.showsScrollIndicator = NO;
	self.direction = LazyScrollableToolbarScrollDirectionHorizontal;
	self.alignment = LazyScrollableToolbarAlignmentCenter;
}

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if ( self ) {
		[self commonInit];
	}
	return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];
	
	[self commonInit];
}

- (void)dealloc {
}




#pragma mark Getter

- (NSArray *)items {
	return [NSArray arrayWithArray:_items];
}

- (BOOL)isIndexValid:(NSInteger)index {
	return ( index >= 0 && _items.count > index );
}




#pragma mark Setter

- (void)setItems:(NSArray *)items {
	for (NSInteger i = 0; i < items.count; i++) {
		id item = items[i];
		NSAssert([item isKindOfClass:[UIButton class]], @"Invalid class for item %ld", (long)i);
	}
	
	// Remove previous items
	for (UIView *item in _items) {
		[item removeFromSuperview];
	}
	
	[_items setArray:items];

	// Add item as subview
	for (UIView *item in _items) {
		[item removeFromSuperview];
		[_internalScrollView addSubview:item];
	}

	// Update layout
	[self updateLayout];
}

- (void)setPadding:(UIEdgeInsets)padding {
	_padding = padding;
	
	// Update layout
	[self updateLayout];
}

- (void)setItemInterval:(CGFloat)itemInterval {
	_itemInterval = itemInterval;
	
	// Update layout
	[self updateLayout];
}

- (void)setItemLength:(CGFloat)itemWidth {
	if ( itemWidth < 0 ) {
		itemWidth = LazyScrollableToolbarItemFlexibleLength;
	}
	
	_itemLength = itemWidth;

//	[self updateScrollViewFrame];
	[self updateLayout];
}


- (void)setEnablePaging:(BOOL)enablePaging {
	_enablePaging = enablePaging;
//	_internalScrollView.pagingEnabled = enablePaging;
	
//	[self updateScrollViewFrame];
}

- (void)setAlignment:(LazyScrollableToolbarAlignment)alignment {
	_alignment = alignment;
	
	// Update layout
	[self updateLayout];
}

- (void)setDirection:(LazyScrollableToolbarScrollDirection)direction {
	_direction = direction;

	if ( _direction == LazyScrollableToolbarScrollDirectionHorizontal ) {
		[_contentViewWidthConstraint uninstall];
		[_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
			_contentViewHeightConstraint = make.height.equalTo(_contentView.superview);
//			_contentViewWidthConstraint = make.width.mas_equalTo(640);
		}];
	} else if ( _direction == LazyScrollableToolbarScrollDirectionVertical ) {
		[_contentViewHeightConstraint uninstall];
		[_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
			_contentViewWidthConstraint = make.width.equalTo(_contentView.superview);
//			_contentViewHeightConstraint = make.height.mas_equalTo(640);
		}];
	}
	
	[self updateScrollIndicator];
	[self updateLayout];
}

- (void)setShowsScrollIndicator:(BOOL)showsScrollIndicator {
	_showsScrollIndicator = showsScrollIndicator;
	
	[self updateScrollIndicator];
}

- (void)setBorderColor:(UIColor *)borderColor {
	_borderColor = borderColor;
	
	[self setNeedsDisplay];
}

- (void)setBorderWidth:(NSUInteger)borderWidth {
	_borderWidth = borderWidth;

	[self setNeedsDisplay];
}




#pragma mark Helper

/**
 *	Insert item to this toolbar
 *	@param		item			Item to be added
 *	@param		index			Index to insert at
 */
- (void)insertItem:(UIButton *)item atIndex:(NSUInteger)index {
	if ( [_items containsObject:item] ) { return; }
	
	[item addTarget:self action:@selector(touchItem:) forControlEvents:UIControlEventTouchUpInside];
	[_items insertObject:item atIndex:index];
	[_internalScrollView addSubview:item];
	
	// Update layout
	[self updateLayout];
}
/**
 *	Add item to this toolbar
 *	@param		item			Item to be added
 */
- (void)addItem:(UIButton *)item {
	item.selected = NO;
	[self insertItem:item atIndex:_items.count];
}



- (void)detachItem:(UIButton *)item {
	[item removeTarget:self action:@selector(touchItem:) forControlEvents:UIControlEventTouchUpInside];
	[item removeFromSuperview];
}
/**
 *	Remove item from this toolbar
 *	@param		index			Index of the item
 */
- (void)removeItemAtIndex:(NSUInteger)index {
	if ( index >= _items.count ) { return; }

	UIButton *item = _items[index];
	[self detachItem:item];
	[_items removeObjectAtIndex:index];
	
	// Update layout
	[self updateLayout];
}
/**
 *	Remove item from this toolbar
 *	@param		item			Item to be removed
 */
- (void)removeItem:(UIButton *)item {
	NSUInteger index = [_items indexOfObject:item];
	[self removeItemAtIndex:index];
}
/**
 *	Remove all the items from this toolbar
 */
- (void)removeAllItems {
	for (UIButton *item in _items) {
		[self detachItem:item];
	}
	[_items removeAllObjects];
	
	// Update layout
	[self updateLayout];
}

- (void)selectItemAtIndex:(NSUInteger)index animated:(BOOL)animated {
	if ( ![self isIndexValid:index] ) {
		return;
	}
	
	if ( index == _previousSelectedIndex ) {
		return;
	}
	
	UIButton *item = nil;
	if ( [self isIndexValid:_previousSelectedIndex] ) {
		// Unselect previous item
		item = _items[_previousSelectedIndex];
		item.selected = NO;
	}
	
	_previousSelectedIndex = index;
	
	// Select new item
	item = _items[_previousSelectedIndex];
	item.selected = YES;
	
	// Scroll to current index
	CGRect frame = CGRectInset(item.frame, -_itemInterval/2, -_itemInterval/2);
	[_internalScrollView scrollRectToVisible:frame animated:animated];
}
- (void)selectItemAtIndex:(NSUInteger)index {
	[self selectItemAtIndex:index animated:NO];
}




#pragma mark Update

- (void)updateScrollIndicator {
	if ( _showsScrollIndicator ) {
		_internalScrollView.showsHorizontalScrollIndicator = ( _direction == LazyScrollableToolbarScrollDirectionHorizontal );
		_internalScrollView.showsVerticalScrollIndicator = ( _direction == LazyScrollableToolbarScrollDirectionVertical );
	} else {
		_internalScrollView.showsHorizontalScrollIndicator = NO;
		_internalScrollView.showsVerticalScrollIndicator = NO;
	}
}

- (void)updateScrollViewFrame {

	// Extend frame of scroll view to parent if not paging
	if ( !_enablePaging ) {
		[_internalScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
			make.edges.equalTo(_internalScrollView.superview);
		}];
		return;
	}
	
	if ( _enablePaging && _itemLength != LazyScrollableToolbarItemFlexibleLength ) {
		if ( _direction == LazyScrollableToolbarScrollDirectionHorizontal ) {
			[_internalScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
				make.top.bottom.centerX.equalTo(_internalScrollView.superview);
				make.width.mas_equalTo(_itemLength);
			}];
		} else if ( _direction == LazyScrollableToolbarScrollDirectionVertical ) {
			[_internalScrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
				make.left.right.centerY.equalTo(_internalScrollView.superview);
				make.height.mas_equalTo(_itemLength);
			}];
		}
	}
}

/**
 *	Update layout of this view
 */
- (void)updateLayout {
	_lastItem = nil;
	
	[_items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		UIButton *item = obj;
		
		// Remove items constraints
		[item removeFromSuperview];
		[_contentView addSubview:item];
		item.tag = idx;
		item.selected = ( idx == _previousSelectedIndex );
		
		if ( _direction == LazyScrollableToolbarScrollDirectionHorizontal ) {
			[item mas_makeConstraints:^(MASConstraintMaker *make) {
				make.top.bottom.equalTo(item.superview);
				
				// Apply width if needed
				if ( _itemLength != LazyScrollableToolbarItemFlexibleLength ) {
					make.width.mas_equalTo(_itemLength);
				}
			}];
			
			// Attach to last item
			if (_lastItem) {
				[item mas_makeConstraints:^(MASConstraintMaker *make) {
					make.left.equalTo(_lastItem.mas_right).offset(_itemInterval);
				}];
			} else {
				[item mas_makeConstraints:^(MASConstraintMaker *make) {
					make.left.equalTo(item.superview).offset(_itemInterval/2);
				}];
			}
		} else if ( _direction == LazyScrollableToolbarScrollDirectionVertical ) {
			[item mas_makeConstraints:^(MASConstraintMaker *make) {
				make.left.right.equalTo(item.superview);
				
				// Apply height if needed
				if ( _itemLength != LazyScrollableToolbarItemFlexibleLength ) {
					make.height.mas_equalTo(_itemLength);
				}
			}];
			
			// Attach to last item
			if (_lastItem) {
				[item mas_makeConstraints:^(MASConstraintMaker *make) {
					make.top.equalTo(_lastItem.mas_bottom).offset(_itemInterval);
				}];
			} else {
				[item mas_makeConstraints:^(MASConstraintMaker *make) {
					make.top.equalTo(item.superview).offset(_itemInterval/2);
				}];
			}
		}
		
		_lastItem = item;
	}];
	
	// Attach border of last item
	if ( _lastItem ) {
		if ( _direction == LazyScrollableToolbarScrollDirectionHorizontal ) {
			[_lastItem mas_makeConstraints:^(MASConstraintMaker *make) {
				make.right.equalTo(_lastItem.superview).offset(-_itemInterval/2);
			}];
		} else if ( _direction == LazyScrollableToolbarScrollDirectionVertical ) {
			[_lastItem mas_makeConstraints:^(MASConstraintMaker *make) {
				make.bottom.equalTo(_lastItem.superview).offset(-_itemInterval/2);
			}];
		}
	}
	
	[self setNeedsLayout];
}




#pragma mark Touch

- (void)touchItem:(UIButton *)sender {
	NSInteger oldIdx = _previousSelectedIndex;
	NSInteger newIdx = sender.tag;
	[self selectItemAtIndex:newIdx animated:YES];
	
	if ( _indexChangeBlock ) {
		_indexChangeBlock(oldIdx, newIdx, sender);
	}
}





#pragma mark UIScrollViewDelegate

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	if ( !_enablePaging ) { return; }
	
	CGFloat kMaxIndex = _items.count - 1;
	if ( kMaxIndex < 0 ) { return; }

	if ( _direction == LazyScrollableToolbarScrollDirectionHorizontal ) {
		CGFloat targetX = scrollView.contentOffset.x + velocity.x * 60.0;
		CGFloat targetIndex = round(targetX / (_itemLength));
		targetIndex = CLAMP(targetIndex, 0, kMaxIndex);
		targetContentOffset->x = targetIndex * (_itemLength);
	}
}

@end
