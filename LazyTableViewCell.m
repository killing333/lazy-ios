//
//  LazyTableViewCell.m
//  MGIA
//
//  Created by Ho Lun Wan on 1/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyTableViewCell.h"

@implementation LazyTableViewCell



#pragma mark Class Method

+ (instancetype)cell {
	return [super create];
}

+ (NSString *)reuseID {
	return NSStringFromClass([self class]);
}

@end
