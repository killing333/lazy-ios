//
//  LazyDevice.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 11/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
	LazyDeviceScaleModeDefault				= 0,			///< No relation between width and height
	LazyDeviceScaleModeAspectToWidth		= 1,			///< Width is scaled with screen width while keeping aspect ratio for the size
	LazyDeviceScaleModeAspectToHeight	= 2,			///< Height is scaled with screen height while keeping aspect ratio for the size
} LazyDeviceScaleMode;

/**
 *	Provide metrics for various devices
 */
@interface LazyDevice : NSObject

+ (instancetype)sharedInstance;

/**
 *	Save config to disk
 */
- (BOOL)saveToDisk;
/**
 *	Load config from disk
 */
- (void)loadFromDisk;

/**
 *	Reset config to default
 */
- (void)reset;

/**
 *	Proportion of default width to default device's width
 */
- (CGFloat)scaleForDefaultWidth:(CGFloat)defaultWidth;
/**
 *	Scaled width for current device as to keep proportion of default device's width to default width
 */
- (CGFloat)scaledWidthForDefaultWidth:(CGFloat)defaultWidth;

/**
 *	Proportion of default height to default device's height
 */
- (CGFloat)scaleForDefaultHeight:(CGFloat)defaultHeight;
/**
 *	Scaled height for current device as to keep proportion of default device's height to default height
 */
- (CGFloat)scaledHeightForDefaultHeight:(CGFloat)defaultHeight;

/**
 *	Scaled size with aspect ratio kept
 */
- (CGSize)scaledSizeForDefaultSize:(CGSize)defaultSize mode:(LazyDeviceScaleMode)mode;

@property (nonatomic) CGSize defaultSize;								///< Size of the default device
@property (nonatomic, readonly) NSInteger applicationLaunchCount;		///< Launch count
@end
