//
//  LazyCollectionViewCell.m
//  MGIA
//
//  Created by Ho Lun Wan on 4/11/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyCollectionViewCell.h"

@implementation LazyCollectionViewCell


#pragma mark Class Method

+ (NSString *)reuseID {
	return NSStringFromClass([self class]);
}


@end
