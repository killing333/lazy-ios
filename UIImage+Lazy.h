//
//  UIImage+Lazy.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 15/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Lazy)

/**
 *	Create an image with pure color
 *	
 *	@param		color			Color of the image
 *	@param		size			Size of the image
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 *	Flip the image horizontally
 */
- (UIImage *)horizontallyFlippedImage;
/**
 *	Flip the image vertically
 */
- (UIImage *)verticallyFlippedImage;

/**
 *	http://stackoverflow.com/questions/5427656/ios-uiimagepickercontroller-result-image-orientation-after-upload
 */
- (UIImage *)fixOrientation;
/**
 *	Return a scaled image of certain size.
 *	Pass 0 to width/height for keeping aspect ratio of the image.
 *	If both width and height are 0, the image does not resize.
 *
 *	@param		size			Size of the new image
 */
- (UIImage *)scaledImageOfSize:(CGSize)newSizsizee;

@end
