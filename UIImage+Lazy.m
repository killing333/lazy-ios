//
//  UIImage+Lazy.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 15/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "UIImage+Lazy.h"

@implementation UIImage (Lazy)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
	// Return nil if color is nil
	if ( color == nil ) {
		return nil;
	}
	
	CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}
+ (UIImage *)imageWithColor:(UIColor *)color {
	return [UIImage imageWithColor:color size:CGSizeMake(1, 1)];
}


- (UIImage *)horizontallyFlippedImage {
	UIImage* flippedImage = [UIImage imageWithCGImage:self.CGImage
												scale:self.scale
										  orientation:UIImageOrientationUpMirrored];
	
	return flippedImage;
}
- (UIImage *)verticallyFlippedImage {
	UIImage* flippedImage = [UIImage imageWithCGImage:self.CGImage
												scale:self.scale
										  orientation:UIImageOrientationLeftMirrored];
	
	return flippedImage;
}

- (UIImage *)fixOrientation {
	
	// No-op if the orientation is already correct
	if (self.imageOrientation == UIImageOrientationUp) return self;
	
	// We need to calculate the proper transformation to make the image upright.
	// We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
	CGAffineTransform transform = CGAffineTransformIdentity;
	
	switch (self.imageOrientation) {
		case UIImageOrientationDown:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, 0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;
			
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, 0, self.size.height);
			transform = CGAffineTransformRotate(transform, -M_PI_2);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationUpMirrored:
			break;
	}
	
	switch (self.imageOrientation) {
		case UIImageOrientationUpMirrored:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.width, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
			
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, self.size.height, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationDown:
		case UIImageOrientationLeft:
		case UIImageOrientationRight:
			break;
	}
	
	// Now we draw the underlying CGImage into a new context, applying the transform
	// calculated above.
	CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
											 CGImageGetBitsPerComponent(self.CGImage), 0,
											 CGImageGetColorSpace(self.CGImage),
											 CGImageGetBitmapInfo(self.CGImage));
	CGContextConcatCTM(ctx, transform);
	switch (self.imageOrientation) {
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			// Grr...
			CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
			break;
			
		default:
			CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
			break;
	}
	
	// And now we just create a new UIImage from the drawing context
	CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
	UIImage *img = [UIImage imageWithCGImage:cgimg];
	CGContextRelease(ctx);
	CGImageRelease(cgimg);
	return img;
}

- (UIImage *)scaledImageOfSize:(CGSize)size {
	if ( size.width <= 0 && size.height <= 0 ) {
		size = self.size;
	} else if ( size.width <= 0 ) {
		CGFloat widthToHeightRatio = self.size.width/self.size.height;
		size.width = size.height * widthToHeightRatio;
	} else if ( size.height <= 0 ) {
		CGFloat heightToWidthRatio = self.size.height/self.size.width;
		size.height = size.width * heightToWidthRatio;
	}

	//UIGraphicsBeginImageContext(newSize);
	// In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
	// Pass 1.0 to force exact pixel size.
	UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
	[self drawInRect:CGRectMake(0, 0, size.width, size.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

@end
