//
//  LazyAnimationQueue.m
//  MGIA
//
//  Created by Ho Lun Wan on 4/1/2016.
//  Copyright © 2016 Wonderful World Group Limited. All rights reserved.
//

#import "LazyAnimationQueue.h"


#define LazyAnimationQueueDictKeyDuration		@"duration"
#define LazyAnimationQueueDictKeyBlock			@"block"


@interface LazyAnimationQueue () {
	NSMutableArray *_animationQueue;
	BOOL _isRunning;
	BOOL _shouldStop;
}
@end


@implementation LazyAnimationQueue

- (instancetype)init {
	self = [super init];
	if ( self ) {
		_animationQueue = [NSMutableArray new];
	}
	
	return self;
}




#pragma mark Class Method

+ (instancetype)sharedInstance {
	static LazyAnimationQueue *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[self class] new];
	});
	
	return sharedInstance;
}





#pragma mark Helper

- (void)addAnimationBlockFromDictionary:(NSDictionary *)dict {
	[_animationQueue addObject:dict];
}
- (void)addAnimationBlock:(void (^)())block duration:(NSTimeInterval)duration {
	[_animationQueue addObject:@{
								 LazyAnimationQueueDictKeyDuration: @(duration),
								 LazyAnimationQueueDictKeyBlock: block,
								 }];
}

- (void)_run {
	if ( _shouldStop ) {
		_shouldStop = NO;
		_isRunning = NO;
		return;
	}
	
	if ( _animationQueue.count == 0 ) {
		_isRunning = NO;
		return;
	}
	
	NSDictionary *dict = _animationQueue.firstObject;
	NSTimeInterval duration = [dict[LazyAnimationQueueDictKeyDuration] doubleValue];
	void (^block)() = dict[LazyAnimationQueueDictKeyBlock];
	
	// Remove the first animation
	[_animationQueue removeObjectAtIndex:0];
	
	[UIView animateWithDuration:duration
						  delay:0
						options:UIViewAnimationOptionCurveEaseIn
					 animations:block
					 completion:^(BOOL finished) {
		[self _run];
	}];
}
- (void)run {
	if ( _isRunning ) {
		return;
	}
	
	_isRunning = YES;
	[self _run];
}

- (void)stop {
	_shouldStop = YES;
}

- (void)clearAllAnimation {
	[_animationQueue removeAllObjects];
}


@end
