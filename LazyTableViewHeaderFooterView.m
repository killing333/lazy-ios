//
//  LazyTableViewHeaderFooterView.m
//  MGIA
//
//  Created by Ho Lun Wan on 2/11/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyTableViewHeaderFooterView.h"

@implementation LazyTableViewHeaderFooterView

+ (NSString *)reuseID {
	return NSStringFromClass([self class]);
}

@end
