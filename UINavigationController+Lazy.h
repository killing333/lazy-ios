//
//  UINavigationController+Lazy.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 6/9/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Lazy)

/**
 *	Pop to controller of certain class which is closest to the root
 */
- (void)popToControllerOfClass:(Class)aClass animated:(BOOL)animated;

@end
