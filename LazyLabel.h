//
//  LazyLabel.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 17/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *	Label which its font change with its height
 */
@interface LazyLabel : UILabel

@property (nonatomic) UIEdgeInsets padding;					///< Padding for text
@property (nonatomic) BOOL adjustFontSizeWithHeight;		///< Auto enlarge/shrink font size with height. Default NO
@property (nonatomic) CGFloat minimunFontSize;				///< Only useful if adjustFontSizeWithHeight is YES
@property (nonatomic) CGFloat maximunFontSize;				///< Only useful if adjustFontSizeWithHeight is YES
@end
