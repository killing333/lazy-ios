//
//  LazyLabel.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 17/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "LazyLabel.h"

@implementation LazyLabel

//- (void)layoutSubviews {
//	[super layoutSubviews];
//}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
	UIEdgeInsets insets = self.padding;
	CGRect rect = [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, insets)
					limitedToNumberOfLines:numberOfLines];
	
	rect.origin.x    -= insets.left;
	rect.origin.y    -= insets.top;
	rect.size.width  += (insets.left + insets.right);
	rect.size.height += (insets.top + insets.bottom);
	
	return rect;
}
- (void)drawTextInRect:(CGRect)rect {
	[self adjustFontSize];
	[super drawTextInRect:UIEdgeInsetsInsetRect(rect, _padding)];
}




#pragma mark Setter

- (void)setAdjustFontSizeWithHeight:(BOOL)adjustFontSizeWithHeight {
	_adjustFontSizeWithHeight = adjustFontSizeWithHeight;
}





#pragma mark Helper

- (void)adjustFontSize {
	
	// Do nothing for no adjustment
	if ( !_adjustFontSizeWithHeight ) {
		return;
	}
	
	CGFloat newFontHeight = CGRectGetHeight(self.bounds);
	
	// Do nothing if font size already the same
//	DLog(@"Current font size: %f", self.font.pointSize);
//	DLog(@"Expected font size: %f", newFontHeight);
	if ( ABS(self.font.pointSize - newFontHeight) <= 1 ) {
		return;
	}
	
	// Do nothing if min > max
	if ( _minimunFontSize > _maximunFontSize ) {
		return;
	}
	
	if ( _minimunFontSize > 0 ) {
		newFontHeight = MAX(_minimunFontSize, newFontHeight);
	}
	if ( _maximunFontSize > 0 ) {
		newFontHeight = MIN(newFontHeight, _maximunFontSize);
	}
	self.font = [self.font fontWithSize:newFontHeight];
}

@end
