//
//  LazyDevice.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 11/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "LazyDevice.h"

NSString *const LazyDeviceDataKeyApplicationLaunchCount			= @"lazy_device-app_launch_count";

@interface LazyDevice () {
	NSUserDefaults *_userDefaults;
}
@end

@implementation LazyDevice

- (instancetype)init {
	self = [super init];
	if ( self ) {
		[self reset];
		
		// Register notification
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunchingNotification:) name:UIApplicationDidFinishLaunchingNotification object:nil];
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)sharedInstance {
	static id sharedInstance = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		sharedInstance = [[[self class] alloc] init];
	});
	
	return sharedInstance;
}




#pragma mark Getter

- (CGFloat)scaleForDefaultWidth:(CGFloat)defaultWidth {
	return defaultWidth / _defaultSize.width;
}
- (CGFloat)scaledWidthForDefaultWidth:(CGFloat)defaultWidth {
	return [UIScreen mainScreen].bounds.size.width * [self scaleForDefaultWidth:defaultWidth];
}

- (CGFloat)scaleForDefaultHeight:(CGFloat)defaultHeight {
	return defaultHeight / _defaultSize.height;
}
- (CGFloat)scaledHeightForDefaultHeight:(CGFloat)defaultHeight {
	return [UIScreen mainScreen].bounds.size.height * [self scaleForDefaultHeight:defaultHeight];
}

- (CGSize)scaledSizeForDefaultSize:(CGSize)defaultSize mode:(LazyDeviceScaleMode)mode {
	CGSize newSize;
	
	if ( mode == LazyDeviceScaleModeDefault ) {
		newSize = CGSizeMake([self scaledWidthForDefaultWidth:defaultSize.width], [self scaledHeightForDefaultHeight:defaultSize.height]);
	} else if ( mode == LazyDeviceScaleModeAspectToWidth ) {
		CGFloat ratio = defaultSize.height / defaultSize.width;
		CGFloat newWidth = [self scaledWidthForDefaultWidth:defaultSize.width];
		newSize = CGSizeMake(newWidth, newWidth * ratio);
	} else if ( mode == LazyDeviceScaleModeAspectToHeight ) {
		CGFloat ratio = defaultSize.width / defaultSize.height;
		CGFloat newHeight = [self scaledHeightForDefaultHeight:defaultSize.height];
		newSize = CGSizeMake(newHeight * ratio, newHeight);
	}
	
	return newSize;
}





#pragma mark Helper

- (BOOL)saveToDisk {
	DLog(@"Save config");
	[_userDefaults setInteger:_applicationLaunchCount forKey:LazyDeviceDataKeyApplicationLaunchCount];
	
	return [_userDefaults synchronize];
}
- (void)loadFromDisk {
	DLog(@"Load config");
	_applicationLaunchCount = [_userDefaults integerForKey:LazyDeviceDataKeyApplicationLaunchCount];
}

- (void)reset {
	_defaultSize = [UIScreen mainScreen].bounds.size;
	_applicationLaunchCount = 0;

	[_userDefaults synchronize];
}




#pragma mark UIApplication Notification

- (void)applicationDidFinishLaunchingNotification:(NSNotification *)notification {
	DLog(@"Application launched");
	_applicationLaunchCount++;
}

@end
