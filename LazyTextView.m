//
//  LazyTextView.m
//  MGIA
//
//  Created by Ho Lun Wan on 29/9/15.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyTextView.h"
#import "LazyProtocolInterceptor.h"


const CGFloat LazyTextViewUnlimitedHeight			= -1.0f;

@interface LazyTextView () <UITextViewDelegate> {
	MASConstraint *_heightConstraint;
	LazyProtocolInterceptor *_protocolInterceptor;
}
@property (nonatomic) CGFloat height;
@end

@implementation LazyTextView

- (void)commonInit {
	_minimumHeight = ( _minimumHeight ? : 30.0f );
	_maximumHeight = ( _maximumHeight ? : LazyTextViewUnlimitedHeight );
	_height = _minimumHeight;
	_adjustsHeightToFitContent = NO;
	_maximumNumberOfCharacters = 0;
	
	// Config placeholder label
	_placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(8,8,self.bounds.size.width - 16,0)];
	_placeholderLabel.lineBreakMode = NSLineBreakByWordWrapping;
	_placeholderLabel.numberOfLines = 0;
	_placeholderLabel.font = self.font;
	_placeholderLabel.backgroundColor = [UIColor clearColor];
	_placeholderLabel.textColor = [UIColor blackColor];
	[self addSubview:_placeholderLabel];
	[_placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.edges.equalTo(_placeholderLabel.superview).insets(UIEdgeInsetsMake(8, 4, 8, 4));
	}];
	
	self.placeholder = _placeholder;
	self.placeholderColor = _placeholderColor;
	
	// Protocol interceptor
	_protocolInterceptor = [[LazyProtocolInterceptor alloc] initWithInterceptedProtocols:@[ @protocol(UITextViewDelegate), ]];
	_protocolInterceptor.middleMan = self;
	[super setDelegate:(id)_protocolInterceptor];
	
	// Register notification
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextDidChangeNotification:) name:UITextViewTextDidChangeNotification object:self];
}

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if ( self ) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if ( self ) {
		[self commonInit];
	}
	return self;
}


- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}





#pragma mark Getter

- (id<UITextViewDelegate>)delegate {
	return _protocolInterceptor.receiver;
}




#pragma mark Setter

- (void)setDelegate:(id<UITextViewDelegate>)delegate {
	[super setDelegate:nil];
	_protocolInterceptor.receiver = delegate;
	[super setDelegate:(id)_protocolInterceptor];;
}

- (void)setText:(NSString *)text {
	[super setText:text];
	[self textViewTextDidChangeNotification:nil];
}

- (void)setAdjustsHeightToFitContent:(BOOL)adjustsHeightToFitContent {
	if ( _adjustsHeightToFitContent == adjustsHeightToFitContent ) {
		return;
	}
	
	_adjustsHeightToFitContent = adjustsHeightToFitContent;
	
	if ( _adjustsHeightToFitContent ) {
		if ( !_heightConstraint ) {
			[self mas_makeConstraints:^(MASConstraintMaker *make) {
				_heightConstraint = make.height.mas_equalTo(_height);
			}];
		} else {
			[_heightConstraint install];
		}
	} else {
		[_heightConstraint uninstall];
	}
}

- (void)setHeight:(CGFloat)height animated:(BOOL)animated {
	if ( !_adjustsHeightToFitContent ) {
		return;
	}
	
	_height = height;
	
	// Update height constraint
	[_heightConstraint setOffset:_height];
	
	if ( animated ) {
		[UIView animateWithDuration:ANIMATION_DURATION animations:^{
			[self.superview layoutIfNeeded];
		} completion:^(BOOL finished) {
			
		}];
	} else {
		[self.superview layoutIfNeeded];
	}
}
- (void)setHeight:(CGFloat)height {
	[self setHeight:height animated:NO];
}

- (void)setMinimumHeight:(CGFloat)minimumHeight {
	_minimumHeight = minimumHeight;
	
	[self updateHeight];
}
- (void)setMaximumHeight:(CGFloat)maximumHeight {
	_maximumHeight = maximumHeight;
	
	[self updateHeight];
}

- (void)setAttributedPlaceholder:(NSAttributedString *)attributedPlaceholder {
	_attributedPlaceholder = attributedPlaceholder;
	_placeholderLabel.attributedText = _attributedPlaceholder;
	[self textViewTextDidChangeNotification:nil];
}
- (void)setPlaceholder:(NSString *)placeholder {
	_placeholder = placeholder;
	
	_placeholderLabel.text = placeholder;
	[self textViewTextDidChangeNotification:nil];
}
- (void)setPlaceholderColor:(UIColor *)placeholderColor {
	_placeholderColor = placeholderColor;
	_placeholderLabel.textColor = placeholderColor;
}

- (void)setFont:(UIFont *)font {
	[super setFont:font];
	_placeholderLabel.font = font;
}




#pragma mark Helper

- (void)clear {
	self.text = nil;
	self.height = _minimumHeight;
}

- (void)updateHeight {
	CGFloat fixedWidth = self.frame.size.width;
	CGSize newSize = [self sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
	CGFloat newHeight = newSize.height;
	
	// Apply min height
	if ( _minimumHeight >= 0 ) {
		newHeight = MAX(newHeight, _minimumHeight);
	}
	
	// Apply max height
	if ( _maximumHeight >= 0 ) {
		newHeight = MIN(newHeight, _maximumHeight);
	}
	
	// Do nothing if height difference is too small
	if ( ABS(_height - newHeight) < 0.1f ) {
		return;
	}
	
	[self setHeight:newHeight animated:YES];
}




#pragma mark UITextViewDelegate



#pragma mark UITextView Notification

- (void)textViewTextDidChangeNotification:(NSNotification *)notification {
	if ( _adjustsHeightToFitContent ) {
		[self updateHeight];
	}
	
	if( self.placeholder.length ) {
		[UIView animateWithDuration:ANIMATION_DURATION animations:^{
			if( [self hasText] ) {
//				_placeholderLabel.alpha = 0.0f;
				_placeholderLabel.hidden = YES;
			} else {
//				_placeholderLabel.alpha = 1.0f;
				_placeholderLabel.hidden = NO;
			}
		}];
	}
	
}


@end
