//
//  CALayer+Lazy.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 8/9/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "CALayer+Lazy.h"

@implementation CALayer (Lazy)

- (void)pauseAnimation {
	CFTimeInterval pausedTime = [self convertTime:CACurrentMediaTime() fromLayer:nil];
	self.speed = 0.0;
	self.timeOffset = pausedTime;
}

- (void)resumeAnimation {
	CFTimeInterval pausedTime = [self timeOffset];
	self.speed = 1.0;
	self.timeOffset = 0.0;
	self.beginTime = 0.0;
	CFTimeInterval timeSincePause = [self convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
	self.beginTime = timeSincePause;
}

@end
