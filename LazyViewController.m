//
//  Q3ViewController.m
//  iOS Commons
//
//  Created by Ho Lun Wan on 22/7/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "LazyViewController.h"
#import "LazyDevice.h"
#import "LazyLanguage.h"

@interface LazyViewController () {
	NSMutableDictionary *_toggleableConstraintsConstantHash;
}

@end

@implementation LazyViewController


- (void)commonInit {
	_viewAppearCount = 0;
	_toggleableConstraintsConstantHash = [NSMutableDictionary new];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lazyLanguageNotificationDidChangeLanguage:) name:LazyLanguageNotificationDidChangeLanguage object:nil];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if ( self ) {
		[self commonInit];
	}
	return self;
}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if ( self ) {
		[self commonInit];
	}
	return self;
}



- (void)awakeFromNib {
	[super awakeFromNib];
}


- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	_isViewLoaded = YES;

	// Update constraints
	LazyDevice *lazyDevice = [LazyDevice sharedInstance];
	for (NSLayoutConstraint *constraint in self.scalableVerticalConstraints) {
		constraint.constant = [lazyDevice scaledHeightForDefaultHeight:constraint.constant];
	}
	for (NSLayoutConstraint *constraint in self.scalableHorizontalConstraints) {
		constraint.constant = [lazyDevice scaledWidthForDefaultWidth:constraint.constant];
	}
	[_toggleableConstraints enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		NSLayoutConstraint *constraint = obj;
		_toggleableConstraintsConstantHash[@(constraint.hash)] = @(constraint.constant);
	}];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self.view setNeedsUpdateConstraints];
	_viewAppearCount++;
	
	// Inform the delegate
	if ( [_delegate respondsToSelector:@selector(controller:willAppear:)] ) {
		[_delegate controller:self willAppear:animated];
	}
}


- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	// Inform the delegate
	if ( [_delegate respondsToSelector:@selector(controller:willDisappear:)] ) {
		[_delegate controller:self willDisappear:animated];
	}
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	_delegate = nil;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





#pragma mark Helper

- (void)updateUI {
	[self.view layoutIfNeeded];
}

- (void)resignFirstResponder {
	[[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}




#pragma mark Toggleable Constraint

- (void)activateToggleableConstraint:(NSLayoutConstraint *)constraint {
	NSNumber *constraintConstant = _toggleableConstraintsConstantHash[@(constraint.hash)];
	if ( constraintConstant != nil ) {
		if ( constraintConstant.doubleValue == 0 ) {
			constraint.active = YES;
		} else {
			constraint.constant = constraintConstant.doubleValue;
		}
	}
}
- (void)deactivateToggleableConstraint:(NSLayoutConstraint *)constraint {
	NSNumber *constraintConstant = _toggleableConstraintsConstantHash[@(constraint.hash)];
	if ( constraintConstant != nil ) {
		if ( constraintConstant.doubleValue == 0 ) {
			constraint.active = NO;
		} else {
			constraint.constant = 0;
		}
	}
}




#pragma mark LazyLanguage Notification

- (void)lazyLanguageNotificationDidChangeLanguage:(NSNotification *)notification {
	[self reloadText];
}

@end
