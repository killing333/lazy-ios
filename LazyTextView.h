//
//  LazyTextView.h
//  MGIA
//
//  Created by Ho Lun Wan on 29/9/15.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>


// Unlimit height for constraint
extern const CGFloat LazyTextViewUnlimitedHeight;

/**
 *	Text view that would resize its height to fit text content
 */
@interface LazyTextView : UITextView

/**
 *	Clear the text and reset the height to minimum
 */
- (void)clear;

/**
 *	Set the height of this view
 */
- (void)setHeight:(CGFloat)height animated:(BOOL)animated;


@property (nonatomic) BOOL adjustsHeightToFitContent;
@property (nonatomic) CGFloat minimumHeight;

/**
 *	Maximum height for expansion. LazyTextViewUnlimitedHeight for unlimited height
 *	Default LazyTextViewUnlimitedHeight
 */
@property (nonatomic) CGFloat maximumHeight;

/**
 *	Maximun number of characters allowed. 0 = unlimited
 *	Default 0
 */
@property (nonatomic) IBInspectable NSInteger maximumNumberOfCharacters;

@property (nonatomic, readonly) UILabel *placeholderLabel;
@property (nonatomic) IBInspectable NSString *placeholder;
@property (nonatomic) NSAttributedString *attributedPlaceholder;
@property (nonatomic) IBInspectable UIColor *placeholderColor;
@end
