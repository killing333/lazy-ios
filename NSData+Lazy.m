//
//  NSData+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 7/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "NSData+Lazy.h"
#import <CommonCrypto/CommonDigest.h> // Need to import for CC_MD5 access

@implementation NSData (Lazy)

- (NSString *)MD5 {
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( self.bytes, (int)self.length, result ); // This is the md5 call
	return [NSString stringWithFormat:
			@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

- (NSString *)hexString {
	NSUInteger capacity = self.length * 2;
	NSMutableString *sbuf = [NSMutableString stringWithCapacity:capacity];
	const unsigned char *buf = self.bytes;
	NSInteger i;
	for (i=0; i<self.length; ++i) {
		[sbuf appendFormat:@"%02lX", (unsigned long)buf[i]];
	}
	
	return sbuf;
}

@end
