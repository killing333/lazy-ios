//
//  UINavigationController+Lazy.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 6/9/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "UINavigationController+Lazy.h"

@implementation UINavigationController (Lazy)

- (void)popToControllerOfClass:(Class)aClass animated:(BOOL)animated {
	UIViewController *targetViewController = nil;
	for (UIViewController *viewController in self.viewControllers) {
		if ( [viewController isMemberOfClass:aClass] ) {
			targetViewController = viewController;
		}
	}
	
	if ( targetViewController ) {
		[self popToViewController:targetViewController animated:animated];
	}
}

@end
