//
//  LazyView.h
//  MGIA
//
//  Created by Ho Lun Wan on 15/1/2016.
//  Copyright © 2016 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LazyView : UIView

/**
 *	Activate the toggleable constraint
 *	It hash no effect if constraint is not assigned to be toggleable
 */
- (void)activateToggleableConstraint:(NSLayoutConstraint *)constraint;
/**
 *	Deactivate the toggleable constraint
 *	It hash no effect if constraint is not assigned to be toggleable
 */
- (void)deactivateToggleableConstraint:(NSLayoutConstraint *)constraint;

/**
 *	When toggled, the constant of constraints will either be set to 0 or restored to original values
 *	If original constant of constraints are already 0, when toggled the constraint will be activated/deactivated
 */
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *toggleableConstraints;
@end
