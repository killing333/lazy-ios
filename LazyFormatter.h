//
//  LazyFormatter.h
//  MGIA
//
//  Created by Ho Lun Wan on 18/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *	Formatter to string for various type
 */
@interface LazyFormatter : NSObject

/**
 *	Convert long value to string in SI format
 */
+ (NSString *)shorthandStringForLongLong:(long long)value;

/**
 *	Return float string with a fixed number of decimal and option of trailing zero
 *
 */
+ (NSString *)stringForFloat:(float)number decimalPlace:(NSInteger)decimalPlace trailingZero:(BOOL)trailingZero;


@end


@interface LazyFormatter (Date)

+ (NSString *)dateSuffixStringForDay:(NSInteger)day;

@end


