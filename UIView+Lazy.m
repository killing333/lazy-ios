//
//  UIView+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 13/10/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "UIView+Lazy.h"

@implementation UIView (Lazy)

+ (UINib *)nib {
	return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (instancetype)create {
	NSArray *views = [[self nib] instantiateWithOwner:nil options:nil];
	id view = nil;
	
	if ( views.count ) {
		view = views[0];
	}
	
	return view;
}

- (id)superviewOfClass:(Class)class {
	UIView *superView = self.superview;
	UIView *foundSuperView = nil;
	
	while (nil != superView && nil == foundSuperView) {
		if ([superView isKindOfClass:class]) {
			foundSuperView = superView;
		} else {
			superView = superView.superview;
		}
	}
	return foundSuperView;
}

- (UIImage *)imageSnapshot {
	UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0);
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	
	UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return img;
}

- (void)reloadText {
	[self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		UIView *view = obj;
		if ( [view respondsToSelector:@selector(reloadText)] ) {
			[view reloadText];
		}
	}];
}

@end
