//
//  LazyCollectionViewCell.h
//  MGIA
//
//  Created by Ho Lun Wan on 4/11/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LazyCollectionViewCell : UICollectionViewCell

/**
 *	Reuse identifier for this cell. Default is the name of this class
 */
+ (NSString *)reuseID;

@end
