//
//  Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 17/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define ARC4RANDOM_MAX      0x100000000


#define CLAMP(v, min, max)									(MIN(max, MAX(min, v)))


/// Short hand for [NSString stringWithFormat]
#define fmtStr(v, ...)										([NSString stringWithFormat:v, ##__VA_ARGS__])

/// Simply print out boolean
#define boolStr(v)											(v ? @"YES" : @"NO")
/// Simply print out integer
#define intStr(v)											fmtStr(@"%d", (int)v)
/// Simply print out long integer
#define longIntStr(v)										fmtStr(@"%ld", (long)v)

/// Get localized error message
#define errStr(k, dv)										NSLocalizedStringWithDefaultValue(k, @"Error", [NSBundle mainBundle], dv, nil)
/// Get error message for certain error code
#define errCodeStr(v)										errStr( intStr(v), locStr(@"general-popup-server_error-title") )
#define attrStr(v, d)										([[NSAttributedString alloc] initWithString:v attributes:d])



#pragma mark Debug

/// Short hand for error logging
/// Reference: http://stackoverflow.com/questions/1451342/objective-c-find-caller-of-method
#ifdef DEBUG
#   define DLog(fmt, ...)							NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif




#pragma mark Setter

#define isNull(v)									( v == nil || [v isKindOfClass:[NSNull class]])
#define setObjectWithDefaults(k, v, d)				if ( isNull(v) ) {k = d;} else {k = v;}
#define setObjectIfNotNil(k, v)						if ( !isNull(v) ) k = v
#define setIntegerIfNotNil(k, v)					if ( !isNull(v) ) k = [v integerValue]
#define setFloatIfNotNil(k, v)						if ( !isNull(v) ) k = [v floatValue]
#define setDoubleIfNotNil(k, v)						if ( !isNull(v) ) k = [v doubleValue]
#define setBoolIfNotNil(k, v)						if ( !isNull(v) ) k = [v boolValue]
#define nilGuardForObject(v)						( isNull(v) ? nil : v )
#define classGuardForObject(v, cls)					( [v isKindOfClass:[cls class]] ? v : nil )




#pragma mark CGPoint

/// Simply log the point value
#define CGPointStr(v)								fmtStr(@"x:%.6f y:%.6f", v.x, v.y)

/**
 *  Difference between the target from origin
 */
UIKIT_STATIC_INLINE CGPoint CGPointDifference(CGPoint origin, CGPoint target) {
    return CGPointMake( origin.x - target.x, origin.y - target.y );
}
/**
 *  Treat CGPoint as vector and calculate its squared length
 */
UIKIT_STATIC_INLINE CGFloat CGPointSquaredLength(CGPoint point) {
    return powf(point.x, 2) + powf(point.y, 2);
}
/**
 *  Treat CGPoint as vector and calculate its length
 */
UIKIT_STATIC_INLINE CGFloat CGPointLength(CGPoint point) {
    return sqrtf(CGPointSquaredLength(point));
}




#pragma mark CGSize

/// Simply log the size value
#define CGSizeStr(v)                                fmtStr(@"w:%.6f h:%.6f", v.width, v.height)





#pragma mark CGRect

/// Simply log the rect value
#define CGRectStr(v)                                fmtStr(@"x:%.6f y:%.6f w:%.6f h:%.6f", v.origin.x, v.origin.y, v.size.width, v.size.height)




#pragma mark Color

#define colorAlpha(r, g, b, a)                      ([UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)/255.0f])
#define color(r, g, b)                              colorAlpha(r, g, b, 255)
#define colorHex(rgbValue)                          color((float)((rgbValue & 0xFF0000) >> 16), (float)((rgbValue & 0x00FF00) >> 8), (float)((rgbValue & 0x0000FF) >>  0))
#define colorHexA(rgbValue)                         colorAlpha((float)((rgbValue & 0xFF000000) >> 24), (float)((rgbValue & 0x00FF0000) >> 16), (float)((rgbValue & 0x0000FF00) >> 8), (float)((rgbValue & 0x000000FF) >>  0))
#define colorPastelRandom                           ([UIColor colorWithHue:arc4random_uniform(255)/255.0f saturation:(0.2f + arc4random_uniform(80)/240.0f) brightness:1 alpha:1])



#pragma mark NSURL
#define url(v)										[NSURL URLWithString:v]



#pragma mark UIImage

/// Quick create UIImage
#define img(v)                                      ([UIImage imageNamed:v])
/// Quick create UIImage with tint
#define tintImg(v)                                  ([img(v) imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate])
#define stillImg(v)                                 ([img(v) imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal])




#pragma mark Index path

#define indexPath(s, r)                             ([NSIndexPath indexPathForRow:r inSection:s])


// #define STATUS_BAR_HEIGHT                           20
// #define NAVIGATION_BAR_HEIGHT                       44
// #define TAB_BAR_HEIGHT                              49
// #define TOOL_BAR_HEIGHT                             44

// Short hand to get the screen size
#define applicationDelegate							([UIApplication sharedApplication].delegate)
#define applicationWindow							([UIApplication sharedApplication].keyWindow)
#define screenSize                                  ([UIScreen mainScreen].bounds.size)
#define screenWidth                                 screenSize.width
#define screenHeight                                screenSize.height
#define statusBarHeight								([UIApplication sharedApplication].statusBarFrame.size.height)
// #define screenHeight_NoNav                          (screenHeight - NAVIGATION_BAR_HEIGHT - STATUS_BAR_HEIGHT)
// #define screenHeight_NoTab                          (screenHeight - TAB_BAR_HEIGHT)
// #define screenHeight_NoNav_NoTab                    (screenHeight - NAVIGATION_BAR_HEIGHT - STATUS_BAR_HEIGHT - TAB_BAR_HEIGHT)
// #define isDevice4S									(MAX(screenHeight, screenWidth) <= 480)
// #define isDevice5									(MAX(screenHeight, screenWidth) <= 568)

#define absoluteLength(v)							(v/[UIScreen mainScreen].scale)



#pragma mark Reader

#define arrayFromPlist(v)							([NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:v ofType:@"plist"]])
#define dictFromPlist(v)							([NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:v ofType:@"plist"]])
