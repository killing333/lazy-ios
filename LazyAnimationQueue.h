//
//  LazyAnimationQueue.h
//  MGIA
//
//  Created by Ho Lun Wan on 4/1/2016.
//  Copyright © 2016 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LazyAnimationQueue : NSObject

+ (instancetype)sharedInstance;

/**
 *	Add an animation to the queue
 */
- (void)addAnimationBlock:(void (^)())block duration:(NSTimeInterval)duration;

/**
 *	Run the animation queue sequentially until it is empty
 */
- (void)run;

/**
 *	Stop running animation
 */
- (void)stop;

- (void)clearAllAnimation;

@end
