//
//  UIView+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 13/10/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Lazy)

/**
 *	NIB for this cell. Default is the name of this class
 */
+ (UINib *)nib;

/**
 *	Create this view from NIB
 */
+ (instancetype)create;

/**
 *	Find a superview of certain class
 *	http://stackoverflow.com/questions/18962771/getting-uitableviewcell-with-superview-in-ios-7
 */
- (id)superviewOfClass:(Class)class;

/**
 *	Snapshot current view and subviews
 *	http://stackoverflow.com/questions/4334233/how-to-capture-uiview-to-uiimage-without-loss-of-quality-on-retina-display
 */
- (UIImage *)imageSnapshot;

/**
 *	Perform any static text reloading in here
 *	This method is heavy, do NOT call frequently
 *
 *	Subclass should call super in overridden method
 */
- (void)reloadText;

@end
