//
//  LazyView.m
//  MGIA
//
//  Created by Ho Lun Wan on 15/1/2016.
//  Copyright © 2016 Wonderful World Group Limited. All rights reserved.
//

#import "LazyView.h"


@interface LazyView () {
	NSMutableDictionary *_toggleableConstraintsConstantHash;
}
@end

@implementation LazyView


- (void)commonInit {
	_toggleableConstraintsConstantHash = [NSMutableDictionary new];
	
	[_toggleableConstraints enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		NSLayoutConstraint *constraint = obj;
		_toggleableConstraintsConstantHash[@(constraint.hash)] = @(constraint.constant);
	}];
}

- (void)awakeFromNib {
	[super awakeFromNib];
	
	[self commonInit];
}

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if ( self ) {
		[self commonInit];
	}

	return self;
}




#pragma mark Helper





#pragma mark Toggleable Constraint

- (void)activateToggleableConstraint:(NSLayoutConstraint *)constraint {
	NSNumber *constraintConstant = _toggleableConstraintsConstantHash[@(constraint.hash)];
	if ( constraintConstant != nil ) {
		if ( constraintConstant.doubleValue == 0 ) {
			constraint.active = YES;
		} else {
			constraint.constant = constraintConstant.doubleValue;
		}
	}
}
- (void)deactivateToggleableConstraint:(NSLayoutConstraint *)constraint {
	NSNumber *constraintConstant = _toggleableConstraintsConstantHash[@(constraint.hash)];
	if ( constraintConstant != nil ) {
		if ( constraintConstant.doubleValue == 0 ) {
			constraint.active = NO;
		} else {
			constraint.constant = 0;
		}
	}
}
@end
