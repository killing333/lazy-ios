//
//  NSArray+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 4/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
	NSArrayPivotModeShrinked		= 0,		///< Sub array would be constrained and shrinked if the pivot hits the border.
	NSArrayPivotModeExpanded		= 1,		///< Sub array would be filled by [NSNull null] if the pivot hits the border. Return size is always [width * 2 + 1]
	NSArrayPivotModePadded			= 2,		///< Sub array would be shifted until the pivot does not hits the nearest border. Return size is always [width * 2 + 1]
} NSArrayPivotMode;

@interface NSArray (Lazy)

/**
 *	Extended functionality for subarrayWithRange:
 */
- (NSArray *)subarrayWithRange:(NSRange)range mode:(NSArrayPivotMode)mode;

/**
 *	Return sub array centered at pivot index with certain width.
 *	The return array is expected as sub array with inclusive range [pivot - width, pivot + width]
 *
 *	@param		pivot				Pivot index
 *	@param		width				Width extended from pivot
 *	@param		mode				@see NSArrayPivotMode
 *	@return		Guaranteed to return array of length [width * 2 + 1] if there is enough elements, else return the whole list
 */
- (NSArray *)subArrayWithPivot:(NSInteger)pivot width:(NSInteger)width mode:(NSArrayPivotMode)mode;
/**
 *	Return sub array centered at pivot index with certain width. 
 *	The return array is expected as sub array with inclusive range [pivot - width, pivot + width]
 *
 *	@param		pivot			Pivot index
 *	@param		width			Width extended from pivot
 *	@return		Guaranteed to return array of length [width * 2 + 1] if there is enough elements, else return the whole list
 */
- (NSArray *)subArrayWithPivot:(NSInteger)pivot width:(NSInteger)width;

@end




@interface NSArray (LazyString)

/**
 *	Return as JSON
 */
- (NSString *)JSONString;

@end