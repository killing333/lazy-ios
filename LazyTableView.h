//
//  LazyTableView.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 5/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LazyTableView;
@protocol LazyTableViewDataSource <NSObject>

@optional
/**
 *	Callback for customizing cell at particular index path
 *	@param		tableView				This lazy table view
 *	@param		cell						Allocated cell for this row
 *	@param		indexPath				Index path of this row
 *	@param		settings					Settings for this row
 */
- (UITableViewCell *)lazyTableView:(LazyTableView *)tableView allocatedCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath withSettings:(NSDictionary *)settings;

@end


@protocol LazyTableViewDelegate <NSObject>

@optional
/**
 *	Tell the receiver user has touched the background of this table view
 */
- (void)didTouchBackgroundInLazyTableView:(LazyTableView *)tableView;

@end



// All of below are optional

/**
 *	Provide either one of them for dequeuing cell from table
 *	If both are provided, LazyTableDataKeyCell would be considered
 */
extern NSString *const LazyTableDataKeyCell;					///< Cell for this row
extern NSString *const LazyTableDataKeyCellID;					///< Cell reuse ID for this row

extern NSString *const LazyTableDataKeyTag;						///< For your own identification of the row
extern NSString *const LazyTableDataKeyTitle;					///< Title for this row
extern NSString *const LazyTableDataKeyDetail;					///< Detail for this row
extern NSString *const LazyTableDataKeyImage;					///< Image for this row
extern NSString *const LazyTableDataKeyUserInfo;				///< Userinfo dictionary for this row
//UIKIT_EXTERN NSString *const LazyTableDataKeyHeight;				///< Height for this row
extern NSString *const LazyTableDataKeyHeader;					///< Header for this row and so on. Must specific this key or LazyTableDataKeyFooter to create a section
extern NSString *const LazyTableDataKeyHeaderTag;				///< Tag of Header for this row
extern NSString *const LazyTableDataKeyFooter;					///< Footer for this row and so on. Must specific this key or LazyTableDataKeyHeader to create a section
extern NSString *const LazyTableDataKeyFooterTag;				///< Tag of Footer for this row


/*
 *	Aims to create static custom table view easily through specifying details of each row 
 *	Inspired by FXForm table creation
 *	https://github.com/nicklockwood/FXForms
 */
@interface LazyTableView : UITableView <UITableViewDataSource, UIGestureRecognizerDelegate> {
	NSMutableArray *_tableDatas;						///< Contains dictionary of key of above
	NSMutableArray *_tableSections;					///< Contains dictionary of key LazyTableDataKeyHeader and LazyTableDataKeyFooter
}

/**
 *	Return settings for row at certain index path
 *	@param		indexPath			Index path for particular row
 */
- (NSDictionary *)settingsForRowAtIndexPath:(NSIndexPath *)indexPath;
/**
 *	Return settings for certain section
 *	@param		section				Index of that section
 */
- (NSDictionary *)settingsForSection:(NSInteger)section;

/**
 *	Table data. Setting this will NOT reload the table
 */
@property (nonatomic) NSArray *data;
@property (nonatomic) BOOL enableAutomaticSectionHeader;			///< Use default style for table section header. Default YES
@property (nonatomic) BOOL enableAutomaticSectionFooter;			///< Use default style for table section footer. Default YES
@property (nonatomic, weak) id<LazyTableViewDataSource> lazyDataSource;
@property (nonatomic, weak) id<LazyTableViewDelegate> lazyDelegate;
@end




