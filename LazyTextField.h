//
//  LazyTextField.h
//  MGIA
//
//  Created by Ho Lun Wan on 4/12/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LazyTextField : UITextField

/**
 *	Maximun number of characters allowed. 0 = unlimited
 *	Default 0
 */
@property (nonatomic) NSInteger maximumNumberOfCharacters;
@end
