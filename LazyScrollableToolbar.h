//
//  LazyScrollableToolbar.h
//  WardrobeApp
//
//  Created by Wan Ho Lun on 23/4/15.
//  Copyright (c) 2015 Gary Lee. All rights reserved.
//

#import <Foundation/Foundation.h>






UIKIT_EXTERN const CGFloat LazyScrollableToolbarItemFlexibleLength;

@protocol LazyScrollableToolbarDelegate <NSObject>

@end

typedef enum : NSUInteger {
	LazyScrollableToolbarAlignmentLeft			= 0,				///< Align items to left. Useless in vertical scroll direction
	LazyScrollableToolbarAlignmentCenter		= 1 << 0,			///< Align items to center.
	LazyScrollableToolbarAlignmentRight			= 1 << 1,			///< Align items to right. Useless in vertical scroll direction
	LazyScrollableToolbarAlignmentTop			= 1 << 2,			///< Align items to top. Useless in horizontal scroll direction
	LazyScrollableToolbarAlignmentBottom		= 1 << 3,			///< Align items to bottom. Useless in horizontal scroll direction
} LazyScrollableToolbarAlignment;

//typedef enum : NSUInteger {
//	LazyScrollableToolbarBorderPositionNone		= 0,
//	LazyScrollableToolbarBorderPositionTop		= 1 << 0,
//	LazyScrollableToolbarBorderPositionBottom		= 1 << 1,
//	LazyScrollableToolbarBorderPositionLeft		= 1 << 2,
//	LazyScrollableToolbarBorderPositionRight		= 1 << 3,
//} LazyScrollableToolbarBorderPosition;

typedef enum : NSUInteger {
	LazyScrollableToolbarScrollDirectionHorizontal		= 0,
	LazyScrollableToolbarScrollDirectionVertical		= 1,
} LazyScrollableToolbarScrollDirection;

/**
 *	Horizontal toolbar
 */
@interface LazyScrollableToolbar : UIView {
	__weak id _delegate;
	NSMutableArray *_items;
}

///**
// *	Insert item to this toolbar
// *	@param		item			Item to be added
// *	@param		index			Index to insert at
// */
//- (void)insertItem:(UIView *)item atIndex:(NSUInteger)index;
/**
 *	Add item to this toolbar
 *	@param		item			Item to be added
 */
- (void)addItem:(UIButton *)item;
///**
// *	Remove item from this toolbar
// *	@param		index			Index of the item
// */
//- (void)removeItemAtIndex:(NSUInteger)index;
///**
// *	Remove item from this toolbar
// *	@param		item			Item to be removed
// */
//- (void)removeItem:(UIView *)item;
/**
 *	Remove all the items from this toolbar
 */
- (void)removeAllItems;

/**
 *	Select item at certain index. If index is invalid, action will be ignored
 *
 *	@param		index			Index of item
 */
- (void)selectItemAtIndex:(NSUInteger)index;
- (void)selectItemAtIndex:(NSUInteger)index animated:(BOOL)animated;


/**
 *	Force to update layout
 */
- (void)updateLayout;


/**
 *	Items in this toolbar. Expected class of or subclass of (UIView *)
 */
@property (strong, nonatomic) NSArray *items;

/**
 *	Titles in this toolbar. Buttons will be created as items automatically for the titles
 */
//@property (strong, nonatomic) NSArray *titles;

/**
 *	Refer to width when scroll direction is horizontal, height when direction is vertical. Use LazyScrollableToolbarItemFlexibleWidth for flexible length, positive value for fixed length.
 *
 *	Default LazyScrollableToolbarItemFlexibleWidth
 */
@property (nonatomic, readwrite) CGFloat itemLength;
/**
 *	Inteval between items
 *
 *	Default 10
 */
@property (nonatomic, readwrite) CGFloat itemInterval;


/**
 *	Whether or not to enable paging to fit length of the item
 *	Only useful when itemLenght is not LazyScrollableToolbarItemFlexibleWidth
 *
 *	Default NO
 */
@property (nonatomic) BOOL enablePaging;

/**
 *	Alignment of items.
 *	
 *	Default LazyScrollableToolbarAlignmentCenter
 */
@property (nonatomic, readwrite) LazyScrollableToolbarAlignment alignment;

/**
 *	Scroll direction.
 *
 *	Default LazyScrollableToolbarScrollDirectionHorizontal
 */
@property (nonatomic, readwrite) LazyScrollableToolbarScrollDirection direction;

/**
 *	Block to be called when index changes. Old index maybe NSNotFound if no item is selected initially
 *
 */
@property (strong, nonatomic) void (^indexChangeBlock)(NSInteger oldIndex, NSInteger newIndex, UIButton *item);

/**
 *	Whether or not to show the scroll indicator depends on the scroll direction
 *
 *	Default NO
 */
@property (nonatomic) BOOL showsScrollIndicator;
@property (weak, nonatomic) id<LazyScrollableToolbarDelegate> delegate;
@end
