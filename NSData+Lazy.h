//
//  NSData+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 7/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Lazy)

- (NSString *)MD5;

/**
 *	http://stackoverflow.com/questions/7520615/how-to-convert-an-nsdata-into-an-nsstring-hex-string
 */
- (NSString *)hexString;

@end
