//
//  UIScrollView+Lazy.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 12/9/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "UIScrollView+Lazy.h"

@implementation UIScrollView (Lazy)





#pragma mark Getter

- (NSInteger)page {
	if ( !self.pagingEnabled ) {
		return -1;
	}
	
	CGFloat pageWidth = self.bounds.size.width;
	float fractionalPage = self.contentOffset.x / pageWidth;
	NSInteger page = lround(fractionalPage);

	return page;
}





#pragma mark Helper

- (void)scrollToPage:(NSInteger)page animated:(BOOL)animated {
	CGFloat pageWidth = self.bounds.size.width;
	CGPoint offset = CGPointMake(page * pageWidth, 0);
	
	[self setContentOffset:offset animated:animated];
}

@end
