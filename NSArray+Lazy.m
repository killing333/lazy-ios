//
//  NSArray+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 4/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "NSArray+Lazy.h"
#import "Lazy.h"

@implementation NSArray (Lazy)

- (NSArray *)subarrayWithRange:(NSRange)range mode:(NSArrayPivotMode)mode {
	NSRange arrayRange = NSMakeRange(0, self.count);
	
	if ( mode == NSArrayPivotModeShrinked ) {
		NSRange intersectionRange = NSIntersectionRange(arrayRange, range);
		return [self subarrayWithRange:intersectionRange];
	} else if ( mode == NSArrayPivotModeExpanded ) {
		
		// Insert NSNull to head and tail for width
		NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:self];
		for (NSInteger i = 0; i < range.length; i++) {
			[tmpArray addObject:[NSNull null]];
		}
		
		return [tmpArray subarrayWithRange:range];
	} else  if ( mode == NSArrayPivotModePadded ) {
		
		// TODO: Padded mode
		return @[];
	}
	
	return @[];
}

- (NSArray *)subArrayWithPivot:(NSInteger)pivot width:(NSInteger)width mode:(NSArrayPivotMode)mode {
	NSInteger subArrayCount = 2 * width + 1;
	if ( mode == NSArrayPivotModeShrinked && subArrayCount >= self.count) {
		return [NSArray arrayWithArray:self];
	}
	
	if ( mode == NSArrayPivotModePadded ) {

		// Pad the pivot if its width hits the boundaries
		NSInteger newPivot = CLAMP(pivot, width, self.count - width - 1);
		return [self subarrayWithRange:NSMakeRange(newPivot - width, subArrayCount)];
	} else if ( mode == NSArrayPivotModeShrinked ) {
		NSInteger start = pivot - width;
		if ( start < 0 ) {
			subArrayCount += start;
			start = 0;
		} else if ( start + subArrayCount > self.count ) {
			subArrayCount -= (start + subArrayCount) - self.count;
		}
		return [self subarrayWithRange:NSMakeRange(start, subArrayCount)];
	} else if ( mode == NSArrayPivotModeExpanded ) {
		
		// Insert NSNull to head and tail for width
		NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:self];
		for (NSInteger i = 0; i < width; i++) {
			[tmpArray insertObject:[NSNull null] atIndex:0];
			[tmpArray addObject:[NSNull null]];
		}
		
		return [tmpArray subarrayWithRange:NSMakeRange(pivot, subArrayCount)];
	}
	
	return nil;
}

- (NSArray *)subArrayWithPivot:(NSInteger)pivot width:(NSInteger)width {
	return [self subArrayWithPivot:pivot width:width mode:NSArrayPivotModeShrinked];
}

@end




@implementation NSArray (LazyString)

- (NSString *)JSONString {
	NSError *error;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
													   options:0
														 error:&error];
	NSString *jsonString = nil;
	
	if (! jsonData) {
		NSLog(@"Error when converting to JSON: %@", error);
	} else {
		jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	}
	
	return jsonString;
}

@end