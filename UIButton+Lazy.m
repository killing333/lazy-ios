//
//  UIButton+Lazy.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 13/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "UIButton+Lazy.h"

@implementation UIButton (Lazy)

- (CGSize)centerImageAndTitleWithSpacing:(float)spacing {
	// lower the text and push it left so it appears centered
	//  below the image
	CGSize imageSize = self.currentImage.size;
	self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);
	
	// raise the image and push it right so it appears centered
	//  above the text
	CGSize titleSize;
	if ( self.currentAttributedTitle ) {
		titleSize = self.currentAttributedTitle.size;
	} else {
		titleSize = [self.currentTitle sizeWithAttributes:@{NSFontAttributeName: self.titleLabel.font}];
	}
	self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
	
	// Update the bounds
	CGSize size = CGSizeMake(CGRectGetWidth(self.bounds), imageSize.height + spacing + titleSize.height + self.contentEdgeInsets.top + self.contentEdgeInsets.bottom);
	return size;
}

- (CGSize)centerImageAndTitle {
	return [self centerImageAndTitleWithSpacing:6.0];
}

@end
