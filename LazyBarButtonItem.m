//
//  LazyBarButtonItem.m
//  MGIA
//
//  Created by Ho Lun Wan on 28/10/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyBarButtonItem.h"


@interface LazyBarButtonItem () {
	UIButton *_innerButton;
	UIView *_contentView;
	
	UIImage *_normalImage;
	UIImage *_selectedlImage;
}

@end

@implementation LazyBarButtonItem

- (instancetype)initWithImage:(UIImage *)image selectedImage:(UIImage *)selectedImage target:(id)target action:(SEL)action {
	_size = CGSizeMake(30, 30);
	_contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _size.width, _size.height)];

	self = [super initWithCustomView:_contentView];
	if ( self ) {
		
		_innerButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[_innerButton setImage:image forState:UIControlStateNormal];
		[_innerButton setImage:selectedImage forState:UIControlStateSelected];
		[_innerButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
		[_contentView addSubview:_innerButton];
		[_innerButton mas_makeConstraints:^(MASConstraintMaker *make) {
			make.edges.equalTo(_innerButton.superview);
		}];

		self.target = target;
		self.action = action;
	}
	
	return self;
}




#pragma mark Setter

- (void)setSize:(CGSize)size {
	_size = size;
	
	_contentView.bounds = CGRectMake(0, 0, _size.width, _size.height);
}

- (void)setSelected:(BOOL)selected {
	_selected = selected;
	_innerButton.selected = selected;
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state {
	[_innerButton setImage:image forState:state];
}


@end
