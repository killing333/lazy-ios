//
//  NSString+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 7/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "NSString+Lazy.h"
#import <CommonCrypto/CommonDigest.h> // Need to import for CC_MD5 access

@implementation NSString (Lazy)

NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

+ (NSString *)randomStringWithLength:(NSInteger)len {
	NSMutableString *randomString = [NSMutableString stringWithCapacity:len];
	
	for (NSInteger i = 0; i < len; i++) {
		[randomString appendFormat: @"%C", [letters characterAtIndex:arc4random_uniform((unsigned int)[letters length])]];
	}
	
	return randomString;
}

- (NSString *)MD5 {
	const char *cStr = [self UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, (int)strlen(cStr), result ); // This is the md5 call
	return [NSString stringWithFormat:
			@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

- (NSString *)stringWithinByteLength:(NSUInteger)length {
	NSData* originalData = [self dataUsingEncoding:NSUTF8StringEncoding];
	
	if ( originalData.length < length ) {
		return self;
	}
	
	const char *originalBytes = originalData.bytes;
	
	//make sure to use a loop to get a not nil string.
	//because your certain length data may be not decode by NSString
	for (NSUInteger i = length; i > 0; i--) {
		
		@autoreleasepool {
			NSData *data = [NSData dataWithBytes:originalBytes length:i];
			NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
			if (string) {
				return string;
			}
		}
		
	}
	return @"";
}

- (NSString *)reverseString {
	NSMutableString *reverse = [NSMutableString new];
	
	for (NSInteger i = self.length - 1; i >= 0 ; i--) {
		[reverse appendString:[self substringWithRange:NSMakeRange(i, 1)]];
	}
	
	return reverse;
}


@end
