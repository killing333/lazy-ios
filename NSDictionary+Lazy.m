//
//  NSDictionary+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 7/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "NSDictionary+Lazy.h"

@implementation NSDictionary (Lazy)

@end




@implementation NSDictionary (LazyString)

- (NSString *)JSONString {
	NSError *error;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
													   options:0
														 error:&error];
	NSString *jsonString = nil;
	
	if (! jsonData) {
		NSLog(@"Error when converting to JSON: %@", error);
	} else {
		jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	}
	
	return jsonString;
}

@end