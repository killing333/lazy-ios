//
//  LazyBarButtonItem.h
//  MGIA
//
//  Created by Ho Lun Wan on 28/10/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *	Bar button that size can be set
 *	However tint color does not apply
 */
@interface LazyBarButtonItem : UIBarButtonItem

/**
 *	Init with image and selected image
 */
- (instancetype)initWithImage:(UIImage *)image selectedImage:(UIImage *)selectedImage target:(id)target action:(SEL)action;

/**
 *	Set image for certain state
 */
- (void)setImage:(UIImage *)image forState:(UIControlState)state;

@property (nonatomic) BOOL selected;			///< Whether or not this button is selected
@property (nonatomic) CGSize size;				///< Size of this button
@end
