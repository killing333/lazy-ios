//
//  UIButton+Lazy.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 13/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Lazy)

/**
 *	Align image above the text
 *	@reference	http://stackoverflow.com/questions/12770751/uibutton-with-title-under-the-imageview
 *	@reference	http://stackoverflow.com/questions/2451223/uibutton-how-to-center-an-image-and-a-text-using-imageedgeinsets-and-titleedgei
 */
- (CGSize)centerImageAndTitleWithSpacing:(float)space;
- (CGSize)centerImageAndTitle;

@end
