//
//  LazyTextField.m
//  MGIA
//
//  Created by Ho Lun Wan on 4/12/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyTextField.h"
#import "LazyProtocolInterceptor.h"


@interface LazyTextField () <UITextFieldDelegate> {
	LazyProtocolInterceptor *_interceptor;
}
@end

@implementation LazyTextField

- (void)commonInit {
	
	// Config delegate interceptor
	_interceptor = [[LazyProtocolInterceptor alloc] initWithInterceptedProtocols:@[ @protocol(UITextFieldDelegate)]];
	_interceptor.middleMan = self;
	[super setDelegate:(id)_interceptor];
	
	_maximumNumberOfCharacters = 0;
}

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if ( self ) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if ( self ) {
		[self commonInit];
	}
	return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];
}





#pragma mark Getter

- (id<UITextFieldDelegate>)delegate {
	return _interceptor.receiver;
}




#pragma mark Setter

- (void)setDelegate:(id<UITextFieldDelegate>)delegate {
	[super setDelegate:nil];
	_interceptor.receiver = delegate;
	[super setDelegate:(id)_interceptor];
}




#pragma mark UITextFieldDelegate

/**
 *	http://stackoverflow.com/questions/433337/set-the-maximum-character-length-of-a-uitextfield
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	// Prevent crashing undo bug – see note below.
	if(range.length + range.location > textField.text.length) {
		return NO;
	}
	
	NSUInteger newLength = [textField.text length] + [string length] - range.length;
	if ( _maximumNumberOfCharacters > 0 && newLength > _maximumNumberOfCharacters ) {
		return NO;
	}
	
	if ( [self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)] ) {
		return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
	} else {
		return YES;
	}
}

@end
