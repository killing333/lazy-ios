//
//  NSURL+Lazy.m
//  MGIA
//
//  Created by Ho Lun Wan on 2/2/2016.
//  Copyright © 2016 Wonderful World Group Limited. All rights reserved.
//

#import "NSURL+Lazy.h"

@implementation NSURL (Lazy)

- (NSDictionary *)queryHash {
	NSMutableDictionary *hash = [NSMutableDictionary new];
	
	NSArray *components = [self.query componentsSeparatedByString:@"&"];
	[components enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		NSString *string = obj;
		NSArray *queryComponents = [string componentsSeparatedByString:@"="];

		if ( queryComponents.count < 2 ) {
			return;
		}
		
		NSString *key = queryComponents[0];
		NSString *value = queryComponents[1];
		if ( hash[key] ) {
			id obj = hash[key];
			NSMutableArray *values = [NSMutableArray new];
			if ( [obj isKindOfClass:[NSArray class]] ) {
				[values setArray:obj];
			} else {
				[values addObject:obj];
			}
			
			hash[key] = values;
		} else {
			hash[key] = value;
		}
	}];
	
	return hash;
}

@end
