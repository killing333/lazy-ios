//
//  NSURL+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 2/2/2016.
//  Copyright © 2016 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Lazy)

/**
 *	Return queries as a dictionary
 */
- (NSDictionary *)queryHash;

@end
