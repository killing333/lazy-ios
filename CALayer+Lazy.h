//
//  CALayer+Lazy.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 8/9/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (Lazy)

/**
 *	Pause CABasicAnimation on this layer
 */
- (void)pauseAnimation;
/**
 *	Resume CABasicAnimation on this layer
 */
- (void)resumeAnimation;

@end
