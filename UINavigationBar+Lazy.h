//
//  UINavigationBar+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 16/11/2015.
//  Copyright © 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Lazy)

/**
 *	Whether or not to show bottom border
 */
- (void)setShowBottomBorder:(BOOL)showBottomBorder;

@end
