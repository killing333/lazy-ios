//
//  NSString+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 7/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Lazy)

/**
 *	Return a random alphanum string with certain length
 */
+ (NSString *)randomStringWithLength:(NSInteger)len;

/**
 *	MD5 of this string
 */
- (NSString *)MD5;

- (NSString *)stringWithinByteLength:(NSUInteger)length;

/**
 *	Reverse of this string
 */
- (NSString *)reverseString;

@end
