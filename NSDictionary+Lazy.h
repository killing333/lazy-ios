//
//  NSDictionary+Lazy.h
//  MGIA
//
//  Created by Ho Lun Wan on 7/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Lazy)

@end



@interface NSDictionary (LazyString)

/**
 *	Return as JSON
 */
- (NSString *)JSONString;

@end