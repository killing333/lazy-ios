//
//  LazyValidation.h
//  ActivBeats
//
//  Created by Ho Lun Wan on 8/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LazyValidation : NSObject

/**
 *  Check if the string is empty
 */
+ (BOOL)isStringEmpty:(NSString *)string;

/**
 *  Check if the string is numeric
 */
+ (BOOL)isStringNumeric:(NSString *)string;

/**
 *  Check if the string is integer
 */
+ (BOOL)isStringInteger:(NSString *)string;

/**
 *  Check if the string is a valid email
 */
+ (BOOL)isStringEmail:(NSString *)string;

/**
 *  Check if the string length is within range
 */
+ (BOOL)isString:(NSString *)string lengthInRange:(NSRange)lengthRange;

@end
