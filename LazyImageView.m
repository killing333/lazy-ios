//
//  LazyImageView.m
//  MGIA
//
//  Created by Ho Lun Wan on 19/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyImageView.h"
#import "Masonry.h"


@interface LazyImageView () {
	MASConstraint *_widthConstraint;
	MASConstraint *_heightConstraint;
}

@end

@implementation LazyImageView

- (void)commonInit {
	MASConstraintMaker *make = [[MASConstraintMaker alloc] initWithView:self];
	_widthConstraint = make.width.mas_equalTo(0);
	_heightConstraint = make.height.mas_equalTo(0);
}
- (instancetype)initWithResizePolicy:(LazyImageViewResizePolicy)policy {
	self = [super init];
	if ( self ) {
		_resizePolicy = policy;
		self.backgroundColor = [UIColor lightGrayColor];
		
		[self commonInit];
	}
	return self;
}
- (void)awakeFromNib {
	[super awakeFromNib];
	
	[self commonInit];
}



#pragma mark Setter

- (void)setResizePolicy:(LazyImageViewResizePolicy)resizePolicy {
	_resizePolicy = resizePolicy;

	if ( _resizePolicy == LazyImageViewResizePolicyFlexibleWidth ) {
		[_widthConstraint install];
		[_heightConstraint uninstall];
	} else if ( _resizePolicy == LazyImageViewResizePolicyFlexibleHeight ) {
		[_widthConstraint uninstall];
		[_heightConstraint install];
	} else {
		[_widthConstraint uninstall];
		[_heightConstraint uninstall];
	}

	[self setNeedsUpdateConstraints];
}

- (void)setImage:(UIImage *)image {
	[super setImage:image];

	[self setNeedsUpdateConstraints];
}




#pragma mark Helper

- (void)updateConstraints {
	[super updateConstraints];
	
	if ( _resizePolicy == LazyImageViewResizePolicyFlexibleWidth ) {
		[self mas_updateConstraints:^(MASConstraintMaker *make) {
			CGFloat width = self.bounds.size.height * (self.image.size.width / self.image.size.height);
			_widthConstraint = make.width.mas_equalTo(width);
		}];
	} else if ( _resizePolicy == LazyImageViewResizePolicyFlexibleHeight ) {
		[self mas_updateConstraints:^(MASConstraintMaker *make) {
			CGFloat height = self.bounds.size.width * (self.image.size.height / self.image.size.width) ;
			_heightConstraint = make.height.mas_equalTo(height);
		}];
	}
}

@end
