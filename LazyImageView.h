//
//  LazyImageView.h
//  MGIA
//
//  Created by Ho Lun Wan on 19/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
	LazyImageViewResizePolicyNone					= 0,
	LazyImageViewResizePolicyFlexibleWidth			= 1,
	LazyImageViewResizePolicyFlexibleHeight			= 2,
} LazyImageViewResizePolicy;

/**
 *	Image view that would auto fit size of the image
 */
@interface LazyImageView : UIImageView

/**
 *	Init this view with a resize policy
 */
- (instancetype)initWithResizePolicy:(LazyImageViewResizePolicy)policy;

@property (nonatomic) LazyImageViewResizePolicy resizePolicy;
@end
