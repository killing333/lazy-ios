//
//  LazyFormatter.m
//  MGIA
//
//  Created by Ho Lun Wan on 18/8/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import "LazyFormatter.h"
#import "Lazy.h"

@implementation LazyFormatter

// http://stackoverflow.com/questions/11993806/convert-int-to-shortened-formatted-string
+ (NSString *)shorthandStringForLongLong:(long long)value {
	NSUInteger index = 0;
	double dvalue = (double)value;
	NSArray *suffix = @[ @"", @"k", @"M", @"G", @"T", @"P", @"E", @"Z", @"Y" ];
	
	while ((value/=1000) && ++index) dvalue /= 1000;
	
	NSString *svalue = [NSString stringWithFormat:@"%.*f%@",
						//Use boolean as 0 or 1 for precision
						(int)(dvalue < 100.0 && ((unsigned)((dvalue - (unsigned)dvalue) * 10) > 0)),
						dvalue, [suffix objectAtIndex:index]];
	
	return svalue;
}

+ (NSString *)stringForFloat:(float)number decimalPlace:(NSInteger)decimalPlace trailingZero:(BOOL)trailingZero {
	NSString *formatString = fmtStr(@"%%.%ldf", (long)decimalPlace);
	NSString *numberString = fmtStr(formatString, number);
	
	if ( !trailingZero ) {
		NSArray *components = [numberString componentsSeparatedByString:@"."];
		if ( components.count > 1 ) {
			NSInteger decimal = [components[1] integerValue];
			if ( decimal == 0 ) {
				numberString = components[0];
			}
		}
	}

	return numberString;
}

@end





@implementation LazyFormatter (Date)

+ (NSString *)dateSuffixStringForDay:(NSInteger)day {
	NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
	NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
	
	return suffixes[day];
}

@end

