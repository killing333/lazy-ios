//
//  LazyValidation.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 8/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "LazyValidation.h"

@implementation LazyValidation

/**
 *  Check if the string is empty
 */
+ (BOOL)isStringEmpty:(NSString *)string {
	return ( string.length == 0 );
}
/**
 *  Check if the string is numeric
 */
+ (BOOL)isStringNumeric:(NSString *)string {
	if ( [[self class] isStringEmpty:string] ) {
		return NO;
	}
	
	NSMutableCharacterSet *nonNumericSet = [NSMutableCharacterSet decimalDigitCharacterSet];
	[nonNumericSet addCharactersInString:@"."];
	[nonNumericSet invert];
	return ([string rangeOfCharacterFromSet:nonNumericSet].location == NSNotFound);
}
/**
 *  Check if the string is integer
 */
+ (BOOL)isStringInteger:(NSString *)string {
	if ( [[self class] isStringEmpty:string] ) {
		return NO;
	}
	
	NSMutableCharacterSet *nonIntegerSet = [NSMutableCharacterSet decimalDigitCharacterSet];
	[nonIntegerSet invert];
	return ([string rangeOfCharacterFromSet:nonIntegerSet].location == NSNotFound);
}
/**
 *  Check if the string is a valid email
 */
+ (BOOL)isStringEmail:(NSString *)string {
	if ( [[self class] isStringEmpty:string] ) {
		return NO;
	}
	
	NSError *error = NULL;
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$" options:NSRegularExpressionCaseInsensitive error:&error];
	NSUInteger numberOfMatch = [regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
	
	return (numberOfMatch == 1);
}
/**
 *  Check if the string length is within range
 */
+ (BOOL)isString:(NSString *)string lengthInRange:(NSRange)lengthRange {
	return NSLocationInRange(string.length, lengthRange);
}


@end
