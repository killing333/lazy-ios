//
//  Q3ViewController.h
//  iOS Commons
//
//  Created by Ho Lun Wan on 22/7/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import <UIKit/UIKit.h>



@class LazyViewController;
@protocol LazyViewControllerDelegate <NSObject>

@optional
/**
 *  Tell the delegate the controller will appear
 *  @param      controller          This controller
 *  @param      animated            Whether or not the appearance is animated
 */
- (void)controller:(LazyViewController *)controller willAppear:(BOOL)animated;
/**
 *  Tell the delegate the controller will disappear
 *  @param      controller          This controller
 *  @param      animated            Whether or not the disappearance is animated
 */
- (void)controller:(LazyViewController *)controller willDisappear:(BOOL)animated;

@end




@interface LazyViewController : UIViewController {
	NSUInteger _viewAppearCount;
	BOOL _isViewLoaded;
	id __weak _delegate;
}

/**
 *	Common Init point
 *	Use viewDidLoad instead
 */
- (void)commonInit NS_DEPRECATED_IOS(7_0, 9_0);

/**
 *	All the UI changing logic should be in here
 *	Call this in any setters if they lead to any UI changes
 */
- (void)updateUI;

/**
 *	Resign first responder in application window
 */
- (void)resignFirstResponder;

@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *scalableVerticalConstraints;				///< Constraints which constant would scale with device's height
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *scalableHorizontalConstraints;			///< Constraints which constant would scale with device's width

/**
 *	When toggled, the constant of constraints will either be set to 0 or restored to original values
 *	If original constant of constraints are already 0, when toggled the constraint will be activated/deactivated
 */
@property (nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *toggleableConstraints;

/**
 *	Activate the toggleable constraint
 *	It hash no effect if constraint is not assigned to be toggleable
 */
- (void)activateToggleableConstraint:(NSLayoutConstraint *)constraint;
/**
 *	Deactivate the toggleable constraint
 *	It hash no effect if constraint is not assigned to be toggleable
 */
- (void)deactivateToggleableConstraint:(NSLayoutConstraint *)constraint;

@property (weak, nonatomic) id<LazyViewControllerDelegate> delegate;
@end
