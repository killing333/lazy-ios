//
//  LazyTableView.m
//  ActivBeats
//
//  Created by Ho Lun Wan on 5/8/15.
//  Copyright (c) 2015 Wan Ho Lun. All rights reserved.
//

#import "LazyTableView.h"

NSString *const LazyTableDataKeyCell			= @"LazyTableDataKeyCell";					///< Cell for this row
NSString *const LazyTableDataKeyCellID			= @"cellID";				///< Cell reuse ID for this row
NSString *const LazyTableDataKeyTag				= @"tag";
NSString *const LazyTableDataKeyTitle			= @"title";					///< Title for this row
NSString *const LazyTableDataKeyDetail			= @"detail";				///< Detail for this row
NSString *const LazyTableDataKeyImage			= @"image";					///< Image for this row
NSString *const LazyTableDataKeyUserInfo		= @"userinfo";				///< Userinfo dictionary for this row
NSString *const LazyTableDataKeyHeight			= @"height";				///< Height for this row
NSString *const LazyTableDataKeyHeader			= @"header";				///< Header for this row and so on
NSString *const LazyTableDataKeyHeaderTag		= @"headerTag";				///< Tag of Header for this row
NSString *const LazyTableDataKeyFooter			= @"footer";				///< Footer for this row and so on
NSString *const LazyTableDataKeyFooterTag		= @"footerTag";				///< Tag of Footer for this row

NSString *const LazyTableDataCellIDDefault	= @"defaultCell";				///< Cell reuse ID for this row



@interface LazyTableViewTapBackgroundGestureRecognizer : UITapGestureRecognizer <UIGestureRecognizerDelegate>

- (instancetype)initInLazyTableView:(LazyTableView *)lazyTableView withTarget:(id)target action:(SEL)action;

@property (nonatomic, readonly) LazyTableView *lazyTableView;
@end

@implementation LazyTableViewTapBackgroundGestureRecognizer

- (instancetype)initInLazyTableView:(LazyTableView *)lazyTableView withTarget:(id)target action:(SEL)action {
	self = [super initWithTarget:target action:action];
	if ( self ) {
		self.numberOfTouchesRequired = 1;
		self.numberOfTapsRequired = 1;
		self.delegate = self;
		_lazyTableView = lazyTableView;
		[lazyTableView addGestureRecognizer:self];
	}
	
	return self;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	BOOL result = YES;
	
	for (UITableViewCell *cell in [_lazyTableView visibleCells]) {
		if ([touch.view isDescendantOfView:cell]) {
			
			// Don't let selections of auto-complete entries fire the
			// gesture recognizer
			result = NO;
		}
	}
	
	return result;
}

@end




#pragma mark -

@interface LazyTableView () {
	LazyTableViewTapBackgroundGestureRecognizer *_tapBackgroundRecognizer;
}

@end

@implementation LazyTableView

- (void)_init {
	_enableAutomaticSectionHeader = YES;
	_enableAutomaticSectionFooter = YES;
	_tableDatas = [NSMutableArray new];
	_tableSections = [NSMutableArray new];

	self.dataSource = self;
	[self registerClass:[UITableViewCell class] forCellReuseIdentifier:LazyTableDataCellIDDefault];
	
	// Add single tap recognizer
	_tapBackgroundRecognizer = [[LazyTableViewTapBackgroundGestureRecognizer alloc] initInLazyTableView:self withTarget:self action:@selector(touchBackground:)];
}

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
	self = [super initWithFrame:frame style:style];
	if ( self ) {
		[self _init];
	}
	return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];
	
	[self _init];
}




#pragma mark Setter

- (void)setData:(NSArray *)data {

	// Clear the data
	[_tableDatas removeAllObjects];
	[_tableSections removeAllObjects];

	NSMutableArray *sectionDatas = [NSMutableArray new];
	NSMutableDictionary *sectionSettings = [NSMutableDictionary new];
	for (NSInteger i = 0; i < [data count]; i++) {
		NSDictionary *settings = data[i];
		
		BOOL isFieldVisible = YES;
		
		// Create section for first item even if header is not specified
		NSString *tmpSectionHeader = settings[LazyTableDataKeyHeader];
		NSString *tmpSectionFooter = settings[LazyTableDataKeyFooter];
		if ( settings[LazyTableDataKeyHeader] != nil || settings[LazyTableDataKeyFooter] != nil ) {

			// End previous section
			if ( i > 0 ) {
				
				// Add section data
				[_tableSections addObject:sectionSettings];
				sectionSettings = [NSMutableDictionary new];
				
				// Add row datas
				[_tableDatas addObject:sectionDatas];
				sectionDatas = [NSMutableArray new];
			}
			
			if ( tmpSectionHeader.length ) {
				sectionSettings[LazyTableDataKeyHeader] = settings[LazyTableDataKeyHeader];
			}
			if ( tmpSectionFooter.length ) {
				sectionSettings[LazyTableDataKeyFooter] = settings[LazyTableDataKeyFooter];
			}
		}
		
		// Finally add the field if it is visible
		if ( isFieldVisible ) {
			[sectionDatas addObject:settings];
		}
	}
	
	[_tableDatas addObject:sectionDatas];
	[_tableSections addObject:sectionSettings];
}





#pragma mark Getter

- (NSDictionary *)settingsForRowAtIndexPath:(NSIndexPath *)indexPath {
	if ( indexPath.section >= _tableDatas.count ) {
		return nil;
	}
	
	NSArray *settingsList = _tableDatas[indexPath.section];
	if ( indexPath.row >= settingsList.count ) {
		return nil;
	}
	
	return settingsList[indexPath.row];
}

- (NSDictionary *)settingsForSection:(NSInteger)section {
	if ( section >= _tableSections.count) {
		return nil;
	}
	
	return _tableSections[section];
}



#pragma mark Touch

- (void)touchBackground:(UITapGestureRecognizer *)tapRecognizer {
	if ( [self.lazyDelegate respondsToSelector:@selector(didTouchBackgroundInLazyTableView:)] ) {
		[self.lazyDelegate didTouchBackgroundInLazyTableView:self];
	}
}

//
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//	[super touchesBegan:touches withEvent:event];
//
//	DLog(@"Touches Began");
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//	[super touchesEnded:touches withEvent:event];
//	
//	DLog(@"Touches Ended");
//}
//


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return _tableSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [(NSArray *)_tableDatas[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *rowSettings = _tableDatas[indexPath.section][indexPath.row];
	BOOL isCustomCell = YES;
	
	// Get custom cell
	UITableViewCell *cell = rowSettings[LazyTableDataKeyCell];
	if ( !cell ) {
		
		// Get cell ID for custom cell
		NSString *cellID = rowSettings[LazyTableDataKeyCellID];
		if ( !cellID.length ) {
			cellID = LazyTableDataCellIDDefault;
			isCustomCell = NO;
		}

		// Dequeue a cell from table
		cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
	}
	
	// Apply settings only on default cell
	if ( !isCustomCell ) {
		cell.textLabel.text = rowSettings[LazyTableDataKeyTitle];
		cell.detailTextLabel.text = rowSettings[LazyTableDataKeyDetail];
		cell.imageView.image = rowSettings[LazyTableDataKeyImage];
	}

	// Allow further customization
	if ( [_lazyDataSource respondsToSelector:@selector(lazyTableView:allocatedCell:forRowAtIndexPath:withSettings:)] ) {
		cell = [_lazyDataSource lazyTableView:(LazyTableView *)tableView allocatedCell:cell forRowAtIndexPath:indexPath withSettings:rowSettings];
	}
	
	return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if ( _enableAutomaticSectionHeader ) {
		NSDictionary *sectionSettings = _tableSections[section];
		return sectionSettings[LazyTableDataKeyHeader];
	} else {
		return nil;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	if ( _enableAutomaticSectionFooter) {
		NSDictionary *sectionSettings = _tableSections[section];
		return sectionSettings[LazyTableDataKeyFooter];
	} else {
		return nil;
	}
}



@end
