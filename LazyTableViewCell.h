//
//  LazyTableViewCell.h
//  MGIA
//
//  Created by Ho Lun Wan on 1/9/15.
//  Copyright (c) 2015 Wonderful World Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Lazy.h"

@interface LazyTableViewCell : UITableViewCell

/**
 *	Reuse identifier for this cell. Default is the name of this class
 */
+ (NSString *)reuseID;

/**
 *	Instance for this cell, created from NIB
 */
+ (instancetype)cell;

@end
